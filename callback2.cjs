/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const lists = require("./lists.json");

function getBoardlist(boardID, callback) {
    const timelag = Math.floor((2 + Math.random() * 1.4) * 1000)
    setTimeout(() => {
        if (boardID in lists) {
            callback(null, lists[boardID])
        }
        else {
            callback(new Error("Board id does not exist"));
        }
    }, timelag)
}

module.exports = getBoardlist;

// getBoardlist("mcu453ed", function (boardList) {
//    console.log(boardList)
// });

