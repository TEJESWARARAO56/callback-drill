/* 
    Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
const boards = require("./boards.json");

const getBoardDetailss = require('./callback1.cjs');
const getBoardlists = require('./callback2.cjs');
const getCardlists = require('./callback3.cjs');

function getBoardinfoAndCardsList(boardname, cardname1, cardname2) {
    const timelag = Math.floor((2 + Math.random() * 1.4) * 1000)
    setTimeout(() => {
        let boardID = boards.find((board) => boardname === board.name);
        if (boardID) {
            boardID = boardID['id']
            getBoardDetailss(boardID, (error, data) => {
                if (error) {
                    console.log(error);
                } else {
                    console.log(data)
                    getBoardlists(boardID, (error, cardlist) => {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log(cardlist)
                            const object1 = cardlist.find((card) => card.name === cardname1);
                            const Id1 = object1['id']
                            const object2 = cardlist.find((card) => card.name === cardname2);
                            const Id2 = object2['id']
                            getCardlists(Id1, (error, cards) => {
                                if (error) {
                                    console.log(error);
                                } else {
                                    console.log({ [object1.name]: cards })
                                    getCardlists(Id2, (error, cards) => {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            console.log({ [object2.name]: cards })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    }, timelag)
}

// getBoardinfoAndCardsList("Thanos", "Mind", "Space");
module.exports = getBoardinfoAndCardsList
