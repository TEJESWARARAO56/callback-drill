/* 
    Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const boards = require("./boards.json");

const getBoardDetailss = require('./callback1.cjs');
const getBoardlists = require('./callback2.cjs');
const getCardlists = require('./callback3.cjs');

function getCardsByBoardName(boardName, listtype) {
    const timelag = Math.floor((2 + Math.random() * 1.4) * 1000)
    setTimeout(() => {
        let boardID = boards.find((board) => boardName === board.name);
        if (boardID) {
            boardID = boardID['id']
            getBoardDetailss(boardID, (error, data) => {
                if (error) {
                    console.log(error);
                } else {
                    console.log(data);
                    getBoardlists(boardID, (error, cardlist) => {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log(cardlist);
                            const mindObject = cardlist.find((card) => card.name === listtype);
                            const mindId = mindObject['id']
                            getCardlists(mindId, (error, data) => {
                                if (error) {
                                    console.log(error);
                                } else {
                                    console.log(data);
                                }
                            })
                        }
                    })
                }
            })
        }
    }, timelag)
}
// getCardsByBoardName('Thanos', 'Mind');
module.exports = getCardsByBoardName