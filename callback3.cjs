/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const cards = require("./cards.json");

function getCardlist(listID, callback) {
    const timelag = Math.floor((2 + Math.random() * 1.4) * 1000)
    setTimeout(() => {
        if (listID in cards) {
            callback(null, cards[listID])
        }
        else {
            callback(new Error("List id does not not exist"));
        }
    }, timelag)
}

// getCardlist("qwsa221", function (boardList) {
//     console.log(boardList)
// });

module.exports = getCardlist;