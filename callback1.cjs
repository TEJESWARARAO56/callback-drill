/* 
    Problem 1: Write a function that will return a particular board's information
    based on the boardID that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/

const boards = require("./boards.json");

function getBoardDetails(boardID, callback) {
    const timelag = Math.floor((2 + Math.random() * 1.4) * 1000)
    setTimeout(() => {
        let boardInfo = boards.find((board) => {
            return board.id === boardID
        });
        if (boardInfo) {
            callback(null, boardInfo);
        } else {
            callback(new Error('Error : Board id does not exist'))
        }
    }, timelag)
}

module.exports = getBoardDetails


// /*
// 	Each function that you write must take at least 2s to execute using the setTimeout function like so:


// setTimeout(() => {
    // Your code here
// }, 2 * 1000);

//     OPTIONAL: You may randomize the time to make things more interesting.
// */

/*
    How the given data is associated:
        Boards have ids and names
        Lists belong to boards
        Cards belong to lists
*/

/*
    Folder structure:
        ├── callback1.cjs
        ├── callback2.cjs
        ├── callback3.cjs
        ├── callback4.cjs
        ├── callback5.cjs
        ├── callback6.cjs
        └── test
            ├── testCallback1.cjs
            ├── testCallback2.cjs
            ├── testCallback3.cjs
            ├── testCallback4.cjs
            ├── testCallback5.cjs
            └── testCallback6.cjs

*/

