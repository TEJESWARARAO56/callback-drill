/* 
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const boards = require("./boards.json");
const getBoardDetailss = require('./callback1.cjs');
const getBoardlists = require('./callback2.cjs');
const getCardlists = require('./callback3.cjs');


function getBoardinfoAndAllCardsList(boardname) {
    const timelag = Math.floor((2 + Math.random() * 1.4) * 1000)
    setTimeout(() => {
        let boardID = boards.find((board) => boardname === board.name);
        if (boardID) {
            boardID = boardID['id']
            getBoardDetailss(boardID, (error, data) => {
                if (error) {
                    console.log(error);
                } else {
                    console.log(data)
                    getBoardlists(boardID, (error, cardlist) => {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log(cardlist)
                            cardlist.forEach(card => {
                                getCardlists(card['id'], (error, cards) => {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        console.log({ [card['name']]: cards })
                                    }
                                })
                            });
                        }
                    })
                }
            })
        }
    }, timelag)
}


// getBoardinfoAndAllCardsList("Thanos");

module.exports = getBoardinfoAndAllCardsList